import fs from 'fs';
import { fileURLToPath } from 'url';
import { dirname } from 'path';
import path from 'path';
const __filename = fileURLToPath(import.meta.url);
const __dirname = dirname(__filename);

export function createFile(req, res, next) {
  try {
    if (!req.body.content || !req.body.filename) {
      res.status(400).json({
        message: `Please specify '${
          req.body.content ? 'filename' : 'content'
        }' parameter`,
      });
    } else {
      if (req.query.pass) {
        const newObj = {
          pass: req.query.pass,
          filename: req.body.filename,
        };
        const passFile = path.join(__dirname, 'files', 'pass.json');
        if (!fs.existsSync(passFile)) {
          fs.writeFileSync(passFile, JSON.stringify({ elements: [newObj] }));
        } else {
          if (
            JSON.parse(fs.readFileSync(passFile, 'utf-8')).elements.filter(
              (item) => {
                return (
                  item.filename === newObj.filename && item.pass === newObj.pass
                );
              }
            ).length > 0
          ) {
            throw new Error();
          } else {
            fs.writeFileSync(
              passFile,
              JSON.stringify({
                elements: JSON.parse(
                  fs.readFileSync(passFile, 'utf-8')
                ).elements.concat(newObj),
              })
            );
          }
        }
      }
      const filePath = path.join(__dirname, 'files', req.body.filename);
      fs.writeFile(filePath, req.body.content, (err) => {
        if (err) {
          throw new Error();
        }
        res.status(200).json({
          message: 'File created successfully',
        });
      });
    }
  } catch (error) {
    next(error);
  }
}

export function getFiles(req, res, next) {
  try {
    const dirPath = path.join(__dirname, 'files');
    const findDir = fs.opendirSync(dirPath);
    if (!findDir) {
      res.status(400).json({
        message: 'Client error',
      });
    } else {
      res
        .status(200)
        .json({ message: 'Success', files: fs.readdirSync(dirPath) });
    }
  } catch (error) {
    next(error);
  }
}

export const getFile = (req, res, next) => {
  try {
    const filePath = path.join(__dirname, 'files', req.params.filename);
    const passFile = path.join(__dirname, 'files', 'pass.json');
    if (!fs.existsSync(filePath)) {
      res.status(400).json({
        message: `No file with '${path.basename(filePath)}' filename found`,
      });
    } else {
      if (fs.existsSync(passFile)) {
        if (req.query.pass) {
          if (
            JSON.parse(fs.readFileSync(passFile, 'utf-8')).elements.filter(
              (item) => {
                return (
                  item.filename === req.params.filename &&
                  item.pass === req.query.pass
                );
              }
            ).length > 0
          ) {
            res.status(200).json({
              message: 'Success',
              filename: req.params.filename,
              content: fs.readFileSync(filePath, 'utf8'),
              extension: path.extname(filePath).slice(1),
              uploadedDate: fs.statSync(filePath).birthtime,
            });
          } else {
            throw new Error();
          }
        } else if (
          JSON.parse(fs.readFileSync(passFile, 'utf-8')).elements.filter(
            (item) => {
              return item.filename === req.params.filename;
            }
          ).length > 0
        ) {
          throw new Error();
        } else {
          res.status(200).json({
            message: 'Success',
            filename: req.params.filename,
            content: fs.readFileSync(filePath, 'utf8'),
            extension: path.extname(filePath).slice(1),
            uploadedDate: fs.statSync(filePath).birthtime,
          });
        }
      } else {
        res.status(200).json({
          message: 'Success',
          filename: req.params.filename,
          content: fs.readFileSync(filePath, 'utf8'),
          extension: path.extname(filePath).slice(1),
          uploadedDate: fs.statSync(filePath).birthtime,
        });
      }
    }
  } catch (error) {
    next(error);
  }
};

export const deleteFile = (req, res, next) => {
  try {
    const filePath = path.join(__dirname, 'files', req.params.filename);
    const passFile = path.join(__dirname, 'files', 'pass.json');
    if (!fs.existsSync(filePath)) {
      throw new Error();
    } else {
      if (req.query.pass) {
        if (
          JSON.parse(fs.readFileSync(passFile, 'utf-8')).elements.filter(
            (item) => {
              if (
                item.filename === req.params.filename &&
                item.pass === req.query.pass
              ) {
                return true;
              }
            }
          ).length > 0
        ) {
          fs.writeFileSync(
            passFile,
            JSON.stringify({
              elements: JSON.parse(
                fs.readFileSync(passFile, 'utf-8')
              ).elements.filter((item) => {
                return item.filename !== req.params.filename;
              }),
            })
          );
          fs.unlinkSync(filePath);
          res.status(200).json({
            message: 'File was deleted!',
            filename: req.params.filename,
            extension: path.extname(filePath).slice(1),
          });
        } else {
          throw new Error();
        }
      } else if (
        JSON.parse(fs.readFileSync(passFile, 'utf-8')).elements.filter(
          (item) => {
            return item.filename === req.params.filename;
          }
        ).length > 0
      ) {
        throw new Error();
      } else {
        fs.unlinkSync(filePath);
        res.status(200).json({
          message: 'File was deleted!',
          filename: req.params.filename,
          extension: path.extname(filePath).slice(1),
        });
      }
    }
  } catch (error) {
    next(error);
  }
};

export const editFile = (req, res, next) => {
  try {
    const filePath = path.join(__dirname, 'files', req.body.filename);
    if (!fs.existsSync(filePath)) {
      res.status(400).json({
        message: `No file with '${req.body.filename}' filename found`,
      });
    } else {
      fs.writeFileSync(filePath, req.body.content);
      res.status(200).json({
        message: 'File was updated!',
        filename: req.body.filename,
        newContent: req.body.content,
      });
    }
  } catch (error) {
    next(error);
  }
};
