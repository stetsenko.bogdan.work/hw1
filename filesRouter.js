import express from 'express';
import {
  createFile,
  deleteFile,
  getFile,
  getFiles,
  editFile,
} from './filesService.js';

const router = express.Router();

//create file
router.post('/files', createFile);

//get files
router.get('/files', getFiles);

//get file by filename
router.get('/files/:filename', getFile);

//delete file
router.delete('/files/:filename', deleteFile);

//modify file
router.put('/files', editFile);

export default router;
